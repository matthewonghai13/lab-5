// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CS378_Lab4/Lab4PlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLab4PlayerController() {}
// Cross Module References
	CS378_LAB4_API UClass* Z_Construct_UClass_ALab4PlayerController_NoRegister();
	CS378_LAB4_API UClass* Z_Construct_UClass_ALab4PlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_CS378_Lab4();
// End Cross Module References
	void ALab4PlayerController::StaticRegisterNativesALab4PlayerController()
	{
	}
	UClass* Z_Construct_UClass_ALab4PlayerController_NoRegister()
	{
		return ALab4PlayerController::StaticClass();
	}
	struct Z_Construct_UClass_ALab4PlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALab4PlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_CS378_Lab4,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALab4PlayerController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Lab4PlayerController.h" },
		{ "ModuleRelativePath", "Lab4PlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALab4PlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALab4PlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALab4PlayerController_Statics::ClassParams = {
		&ALab4PlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALab4PlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALab4PlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALab4PlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALab4PlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALab4PlayerController, 3725145771);
	template<> CS378_LAB4_API UClass* StaticClass<ALab4PlayerController>()
	{
		return ALab4PlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALab4PlayerController(Z_Construct_UClass_ALab4PlayerController, &ALab4PlayerController::StaticClass, TEXT("/Script/CS378_Lab4"), TEXT("ALab4PlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALab4PlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
