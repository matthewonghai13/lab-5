// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Character.h"

// Sets default values
ALab4Character::ALab4Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MoveSpeed = 300.0f;
	TimePassed = 0.f;
	TimeLastInteracted = 100.f;
}

// Called when the game starts or when spawned
void ALab4Character::BeginPlay()
{
	Super::BeginPlay();
	if(GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, TEXT("		STATE: IDLE"));
	}
	CurrentState = idle;
	EncodedState = 1;
}

// Called every frame
void ALab4Character::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

	TimePassed += DeltaTime;
	
	// automatically return to idle after interacting
	if(((TimeLastInteracted + 3.f < TimePassed + 0.1) && (TimeLastInteracted + 3.f > TimePassed - 0.1)) &&
	CurrentState == interact) {
		CurrentState = idle;
	}

	// if(CurrentState == idle) {
	// 	EncodedState = 0;
	// } else if(CurrentState == move) {
	// 	EncodedState = 1;
	// } else if(CurrentState == jump) {
	// 	EncodedState = 2;
	// } else if(CurrentState == interact) {
	// 	EncodedState = 3;
	// }
}

// Called to bind functionality to input
void ALab4Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ALab4Character::JumpPressed() {
	const FVector MoveDirection = FVector(0.0f, 0.f, 10.f).GetClampedToMaxSize(1.0f);
	const FVector Movement = MoveDirection * MoveSpeed;

	// If valid jump, move this actor
	if ((Movement.SizeSquared() > 0.0f) && (CanPerformAction(jump)))
	{
		const FRotator NewRotation = Movement.Rotation();
		FHitResult Hit(1.f);
		RootComponent->MoveComponent(Movement, NewRotation, false, &Hit);
		CurrentState = jump;

		if (Hit.IsValidBlockingHit())
		{
			const FVector Normal2D = Hit.Normal.GetSafeNormal2D();
			const FVector Deflection = FVector::VectorPlaneProject(Movement, Normal2D) * (1.f - Hit.Time);
			RootComponent->MoveComponent(Deflection, NewRotation, false);
		}
	}
}

void ALab4Character::InteractPressed() {

	if(CanPerformAction(interact)) {
		if(GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::White, TEXT("STATE: INTERACTING"));
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("INTERACT (E) PRESSED"));
		}
		TimeLastInteracted = TimePassed;
		CurrentState = interact;
	}	
}

void ALab4Character::Move(float ForwardValue, float RightValue, float DeltaSeconds) {

	if(CurrentState != move) {
		Prev = CurrentState;
	}

	const FVector MoveDirection = FVector(ForwardValue, RightValue, 0.f).GetClampedToMaxSize(1.0f);
	const FVector Movement = MoveDirection * MoveSpeed * DeltaSeconds;

	// If non-zero size, move this actor
	if ((Movement.SizeSquared() > 0.0f) && (CanPerformAction(move)))
	{
		// if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::White, TEXT("		STATE: MOVING"));
		const FRotator NewRotation = Movement.Rotation();
		FHitResult Hit(1.f);
		RootComponent->MoveComponent(Movement, NewRotation, true, &Hit);
		CurrentState = move;
		
		if (Hit.IsValidBlockingHit())
		{
			const FVector Normal2D = Hit.Normal.GetSafeNormal2D();
			const FVector Deflection = FVector::VectorPlaneProject(Movement, Normal2D) * (1.f - Hit.Time);
			RootComponent->MoveComponent(Deflection, NewRotation, true);
		}
	} else {
		// if not moving, must be idle jump or interact
		CurrentState = Prev;
	}
}

bool ALab4Character::CanPerformAction(ALab4Character::PlayerState UpdatedAction) {
	// can do anything while idling
	if(CurrentState == idle) return true;

	// cannot interact while moving or jumping
	if((CurrentState == jump || CurrentState == move) && (UpdatedAction == interact)) {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("BLOCKED; ATTEMPTED TO INTERACT WHILE MOVING OR JUMPING"));
		return false;
	}

	if((CurrentState == interact) && ((UpdatedAction == move) || (UpdatedAction == jump))) {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("BLOCKED; ATTEMPTED TO PERFORM ACTION WHILE INTERACTING"));
		return false;
	}
	

	return true;
}

void ALab4Character::UpdateActionState(ALab4Character::PlayerState NewAction) {
	CurrentState = NewAction;
}