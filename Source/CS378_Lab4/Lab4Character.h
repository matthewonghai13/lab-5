// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Lab4Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum : uint8 {
	IDLE UMETA(DisplayName = "Idling"),
	MOVE UMETA(DisplayName = "Moving"),
	JUMP UMETA(DisplayName = "Jumping"),
	INTERACT UMETA(DisplayName = "Interacting"),
};

UCLASS()
class CS378_LAB4_API ALab4Character : public ACharacter
{
	GENERATED_BODY()


public:
	// Sets default values for this character's properties
	ALab4Character();
	float MoveSpeed;
	float TimePassed;
	float TimeLastInteracted;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	// UPROPERTY()
	// ECharacterStateEnum CharacterActionState;
	enum PlayerState {idle, move, jump, interact};
					// 0, 1, 2, 3, 4
	
	PlayerState CurrentState;
	PlayerState Prev;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int EncodedState;

	// UFUNCTION(BlueprintCallable)
	bool CanPerformAction(PlayerState UpdatedAction);

	// UFUNCTION(BlueprintCallable)
	void UpdateActionState(PlayerState NewAction);


	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void InteractPressed();

	void JumpPressed();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
		
	void Move(float ForwardValue, float RightValue, float DeltaSeconds);

};


