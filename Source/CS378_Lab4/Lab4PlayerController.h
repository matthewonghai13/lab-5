// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/PlayerController.h"
#include "Lab4Character.h"
#include "Lab4PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB4_API ALab4PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ALab4PlayerController();
	~ALab4PlayerController();


private:
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaSeconds) override;
	void JumpPressed();
	void InteractPressed();
};
