// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab4Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Lab4PlayerController.h"
#include "Engine.h"
#include "CS378_Lab4GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB4_API ACS378_Lab4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACS378_Lab4GameModeBase();
	~ACS378_Lab4GameModeBase();

};
