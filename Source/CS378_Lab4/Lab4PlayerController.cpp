// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4PlayerController.h"

ALab4PlayerController::ALab4PlayerController() {
}

ALab4PlayerController::~ALab4PlayerController() {

}

void ALab4PlayerController::SetupInputComponent() {
    Super::SetupInputComponent();

    check(InputComponent);

    InputComponent->BindAction("Jump", IE_Pressed, this, &ALab4PlayerController::JumpPressed);
    InputComponent->BindAction("Interact", IE_Pressed, this, &ALab4PlayerController::InteractPressed);

    InputComponent->BindAxis("MoveForwardsBackwards");
    InputComponent->BindAxis("MoveLeftRight");
}

void ALab4PlayerController::Tick(float DeltaSeconds) {
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character) {
        character->Move(GetInputAxisValue("MoveForwardsBackwards"), GetInputAxisValue("MoveLeftRight"), DeltaSeconds);
    }
}

void ALab4PlayerController::JumpPressed() {
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character) {
        character->JumpPressed();
    }
}

void ALab4PlayerController::InteractPressed() {
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character) {
        character->InteractPressed();
    }    
}