// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_Lab4GameModeBase.h"

ACS378_Lab4GameModeBase::ACS378_Lab4GameModeBase() {
    static ConstructorHelpers::FObjectFinder<UClass> charBPClass(TEXT("Blueprint'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'"));
	if(charBPClass.Object) {
		if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("CHARBP FOUND"));
		UClass* charBP = (UClass* )charBPClass.Object;
		DefaultPawnClass = charBP;
	} else {
		if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("CHARBP NOT FOUND"));
		DefaultPawnClass = ALab4Character::StaticClass();
	}

	PlayerControllerClass = ALab4PlayerController::StaticClass();
}

ACS378_Lab4GameModeBase::~ACS378_Lab4GameModeBase() {

}